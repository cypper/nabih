import { Сурядина } from "./сустаті/сурядина";

type ДаніНабігу = {
  кількість: number;
};

export class НабігоПороджувач {
  вкПотНабігу = -1;
  constructor(
    readonly сурядина: Сурядина,
    private набіги: ДаніНабігу[],
  ) {}

  початиНабіги() {
    this.вкПотНабігу = 0;
  }

  настНабіг() {
    if (this.набіги[this.вкПотНабігу + 1] === undefined) return null;

    this.вкПотНабігу++;
    return this.потНабіг;
  }

  get потНабіг() {
    return this.набіги[this.вкПотНабігу] ?? null;
  }
}
