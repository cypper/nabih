import * as tf from "@tensorflow/tfjs-node";
import { ЗаймачМісця } from "../../поле";

type ВхДані = (ЗаймачМісця | undefined)[]; // займач, дальність, кут
type ВихДані = "пн" | "пд" | "зх" | "сх";

export class РозумВорога {
  private readonly розмірВхіднихДаних: number;
  private readonly тварВхіднихДаних: tf.ShapeMap[tf.Rank.R2];
  private readonly розмірВихіднихДаних: number;
  private readonly тварВихіднихДаних: tf.ShapeMap[tf.Rank.R1];
  private readonly бгал: tf.Sequential;

  constructor(private readonly обсягЗору: number) {
    this.розмірВхіднихДаних = this.тварТаРозмірЧутників(обсягЗору).розмір;
    this.тварВхіднихДаних = this.тварТаРозмірЧутників(обсягЗору).твар;

    this.розмірВихіднихДаних = this.тварТаРозмірДанихВолі().розмір;
    this.тварВихіднихДаних = this.тварТаРозмірДанихВолі().твар;

    this.бгал = this.створитиБгал();
  }

  віщувати(вхДані: ВхДані): ВихДані {
    const віщаКР = this.бгал.predict(
      this.вВхДаніКР(вхДані).as3D(1, ...this.тварВхіднихДаних),
    );
    if (Array.isArray(віщаКР)) throw new Error("Неочікуваний масив");
    return this.зВихДанихКР(віщаКР.as1D());
  }

  async човпити(вхДані: ВхДані, видинніВихДані: ВихДані) {
    const злученіВхДаніКР = this.вВхДаніКР(вхДані).as3D(
      1,
      ...this.тварВхіднихДаних,
    );
    const злученіВихДаніКР = this.вВихДаніКР(видинніВихДані).as2D(
      1,
      ...this.тварВихіднихДаних,
    );

    await this.бгал.fit(злученіВхДаніКР, злученіВихДаніКР, {
      epochs: 10,
      shuffle: true,
    });
  }

  private вВхДаніКР(дані: ВхДані) {
    return tf.tensor(
      дані.map((д) => [д === undefined ? 1 : 0, д === null ? 1 : 0, д ? 1 : 0]),
      this.тварВхіднихДаних,
    );
  }

  private вВихДаніКР(дані: ВихДані) {
    return tf.tensor(
      ["пн", "пд", "зх", "сх"].map((щ) => (щ === дані ? 1 : 0)),
      this.тварВихіднихДаних,
    );
  }

  private зВихДанихКР(вихДаніКР: tf.Tensor<tf.Rank.R1>) {
    const вислід = вихДаніКР.arraySync();
    const вкНаййм = вислід.indexOf(Math.max(...вислід));
    return ["пн", "пд", "зх", "сх"][вкНаййм] as ВихДані;
  }

  private створитиБгал() {
    const бгал = tf.sequential();
    бгал.add(
      tf.layers.inputLayer({
        inputShape: this.тварВхіднихДаних,
      }),
    );
    бгал.add(
      tf.layers.reshape({
        targetShape: [this.розмірВхіднихДаних],
      }),
    );
    бгал.add(
      tf.layers.dense({
        units: Math.round(this.розмірВихіднихДаних),
        activation: "selu",
      }),
    );
    бгал.add(
      tf.layers.dense({
        units: this.розмірВихіднихДаних,
        activation: `sigmoid`,
      }),
    );
    бгал.add(
      tf.layers.reshape({
        targetShape: this.тварВихіднихДаних,
      }),
    );

    this.укласти(бгал);

    return бгал;
  }

  private укласти(бгал: tf.Sequential) {
    бгал.compile({
      loss: tf.losses.huberLoss,
      optimizer: tf.train.adamax(0.01),
      metrics: [],
    });
  }

  private тварТаРозмірЧутників(обсягЗору: number) {
    return {
      розмір: обсягЗору * 3,
      твар: [обсягЗору, 3] as [number, number],
    } as const;
  }

  private тварТаРозмірДанихВолі() {
    return {
      розмір: 4,
      твар: [4] as [number],
    } as const;
  }
}
