export class Носій {
  #за: number;
  #зб: number;
  constructor(за: number, зб: number) {
    this.#за = за;
    this.#зб = зб;
  }

  узвичити() {
    const найбЗн = Math.max(this.за, this.зб);
    return new Носій(this.за / найбЗн, this.зб / найбЗн);
  }

  кут(н: Носій) {
    return Math.acos(this.скалярнийДобуток(н) / (this.довжина() * н.довжина()));
  }

  скалярнийДобуток(н: Носій) {
    return this.за * н.за + this.зб * н.зб;
  }

  довжина() {
    return Math.sqrt(this.за ** 2 + this.зб ** 2);
  }

  get за() {
    return this.#за;
  }
  private set за(зн: number) {
    this.#за = зн;
  }

  get зб() {
    return this.#зб;
  }
  private set зб(зн: number) {
    this.#зб = зн;
  }
}
