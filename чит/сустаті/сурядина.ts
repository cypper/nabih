import { Носій } from "./носій";

export class Сурядина {
  #а: number;
  #б: number;
  constructor(а: number, б: number) {
    this.#а = а;
    this.#б = б;
  }

  різниця(с: Сурядина) {
    return new Носій(с.а - this.а, с.б - this.б);
  }

  перенести(н: Носій) {
    const а = this.а + н.за;
    const б = this.б + н.зб;
    return new Сурядина(а, б);
  }

  рівне(с: Сурядина) {
    return this.а === с.а && this.б === с.б;
  }

  get а() {
    return this.#а;
  }
  private set а(зн: number) {
    this.#а = зн;
  }

  get б() {
    return this.#б;
  }
  private set б(зн: number) {
    this.#б = зн;
  }
}
