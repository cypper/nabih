import { Сурядина } from "./сустаті/сурядина";

export class Осіднище {
  constructor(
    readonly сурядина: Сурядина,
    private здоровля: number,
  ) {}

  нищити(силаНищі: number) {
    this.здоровля -= силаНищі;
  }

  чиЗнищенне() {
    return this.здоровля <= 0;
  }
}
